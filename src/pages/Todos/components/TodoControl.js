import React from 'react';
import PropTypes from 'prop-types';

const TodoControl = props => (
    <React.Fragment>
        <span className="todo-control close" onClick={() => props.deleteTodo(props.id)} title="Remove"/>
        {!props.completed && !props.edit ?
            <span className="todo-control complete" onClick={() => props.completeTodo(props.id)} title="Complete"/> : null}
        {props.showSave ?
            <span className="todo-control save" onClick={() => props.editTodo(props.id)} title="Save Changes"/> : null}
    </React.Fragment>
);

TodoControl.propTypes = {
    id: PropTypes.number.isRequired,
    completeTodo: PropTypes.func.isRequired,
    deleteTodo: PropTypes.func.isRequired,
    editTodo: PropTypes.func.isRequired,
    completed: PropTypes.bool.isRequired,
    showSave: PropTypes.bool,
    edit: PropTypes.bool
};

export default TodoControl;
