import React, {Component} from 'react';
import PropTypes from 'prop-types';

class TodoAdd extends Component {

    constructor(props) {
        super(props);
        this.state = {
            name: ""
        }
    }

    handleInput(e) {
        this.setState({name: e.target.value})
    }

    render() {
        const {name} = this.state;
        return (
            <div className="todo-add">
                <input value={name} onChange={(e) => this.handleInput(e)}/>
                {name.length > 0 ? <span className="todo-control new" onClick={() => this.props.addTodo(name)} title="Add"/> : null}
            </div>
        )
    }
}

TodoAdd.propTypes = {
    addTodo: PropTypes.func.isRequired
};

export default TodoAdd;
