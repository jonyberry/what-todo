import React from 'react';
import PropTypes from 'prop-types';

const TodoStats = props => {
    const completed = props.completedTodos.length;
    return <div className="todo-stats">
        <p>TODO: {completed}</p>
        {props.todos.length !== completed ?
            <p className="cleaner" onClick={() => props.closeCompletedTodos(props.completedTodos)}>
                Remove all completed </p> : null}
    </div>
};

TodoStats.propTypes = {
    todos: PropTypes.arrayOf(
        PropTypes.shape({
            name: PropTypes.string,
            completed: PropTypes.bool
        })
    ).isRequired,
    completedTodos: PropTypes.arrayOf(
        PropTypes.shape({
            name: PropTypes.string,
            completed: PropTypes.bool
        })
    ).isRequired,
    closeCompletedTodos: PropTypes.func.isRequired
};

export default TodoStats;
