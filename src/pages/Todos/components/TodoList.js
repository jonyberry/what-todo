import React, {Component} from 'react';
import PropTypes from 'prop-types';
import TodoItem from './TodoItem';

class TodoList extends Component {

    render() {
        return (
            <div className="todo-list">
                {this.props.todos.map((todo, index) => {
                    return <TodoItem
                        key={index}
                        id={index}
                        completeTodo={(id) => this.props.completeTodo(id)}
                        deleteTodo={(id) => this.props.deleteTodo(id)}
                        editTodo={(id, text) => this.props.editTodo(id, text)}
                        name={todo.name}
                        completed={todo.completed}
                    />
                })}
            </div>
        );
    }
}

TodoList.propTypes = {
    todos: PropTypes.arrayOf(
        PropTypes.shape({
            name: PropTypes.string,
            completed: PropTypes.bool
        })
    ).isRequired,
    completeTodo: PropTypes.func.isRequired,
    deleteTodo: PropTypes.func.isRequired,
    editTodo: PropTypes.func.isRequired
};

export default TodoList;
