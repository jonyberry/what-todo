import React, {Component} from 'react';
import TodoAdd from './components/TodoAdd';
import TodoStats from './components/TodoStats';
import TodoList from './components/TodoList';
import './todos.css';

class Todos extends Component {

    componentDidMount() {
        this.props.fetchTodos();
    }

    render() {
        return (
            <div className="todos">
                <TodoAdd addTodo={this.props.addTodo}/>
                <TodoStats todos={this.props.todos}
                           completedTodos={this.props.todos.filter(todo => !todo.completed)}
                           closeCompletedTodos={this.props.closeCompletedTodos}/>
                <TodoList {...this.props}/>
            </div>
        );
    }
}

export default Todos;
