import {connect} from 'react-redux';
import Todos from './Todos';
import {closeCompletedTodos, deleteTodo, completeTodo, editTodo, fetchTodos, addTodo} from '../../modules/todos';
import {bindActionCreators} from 'redux';

const mapStateToProps = store => ({
    todos: store.todos.todos,
});

const mapDispatchToProps = dispatch => (
    bindActionCreators({fetchTodos, addTodo, deleteTodo, editTodo, completeTodo, closeCompletedTodos}, dispatch)
);

export default connect(mapStateToProps, mapDispatchToProps)(Todos);
