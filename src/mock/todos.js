export const todos = [
    {
        name: "Push repo",
        completed: true
    },
    {
        name: "Track time",
        completed: false,
    },
    {
        name: "Be better",
        completed: false,
    }
];