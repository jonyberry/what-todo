import {todos} from '../mock/todos';

const FETCH_TODOS = "FETCH_TODOS";
const ADD_TODO = "ADD_TODO";
const DELETE_TODO = "DELETE_TODO";
const EDIT_TODO = "EDIT_TODO";
const COMPLETE_TODO = "COMPLETE_TODO";
const CLOSE_COMPLETED_TODOS = "CLOSE_COMPLETED_TODOS";

export const initialState = {
    todos: []
};

export default function reducer(state = initialState, action) {
    switch (action.type) {
        case FETCH_TODOS: {
            return {
                ...state,
                todos: action.payload
            }
        }
        case ADD_TODO: {
            return {
                ...state,
                todos: [...state.todos, action.payload]
            }
        }
        case DELETE_TODO: {
            return {
                ...state,
                todos: [...state.todos.slice(0, action.payload), ...state.todos.slice(action.payload + 1)]
            }
        }
        case EDIT_TODO: {
            return {
                ...state,
                todos: [
                    ...state.todos.slice(0, action.payload.index),
                    {name: action.payload.name, completed: state.todos[action.payload.index].completed},
                    ...state.todos.slice(action.payload.index + 1)
                ]
            }
        }
        case COMPLETE_TODO: {
            return {
                ...state,
                todos: [
                    ...state.todos.slice(0, action.payload),
                    {name: state.todos[action.payload].name, completed: true},
                    ...state.todos.slice(action.payload + 1)
                ]
            }
        }
        case CLOSE_COMPLETED_TODOS: {
            return {
                ...state,
                todos: action.payload
            }
        }
        default: {
            return state
        }
    }
}

// data mock in src/mock/todos
export function fetchTodos() {
    return {
        type: FETCH_TODOS,
        payload: todos
    }
}

export function addTodo(name) {
    return {
        type: ADD_TODO,
        payload: {name, completed: false}
    }
}

export function deleteTodo(index) {
    return {
        type: DELETE_TODO,
        payload: index
    }
}

export function editTodo(index, name) {
    return {
        type: EDIT_TODO,
        payload: {index, name}
    }
}

export function completeTodo(index) {
    return {
        type: COMPLETE_TODO,
        payload: index
    }
}

export function closeCompletedTodos(newTodos) {
    return {
        type: CLOSE_COMPLETED_TODOS,
        payload: newTodos
    }
}




