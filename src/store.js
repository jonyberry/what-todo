import { combineReducers, createStore, applyMiddleware } from 'redux'
import { createLogger } from 'redux-logger'
import todos from './modules/todos';

const reducer = combineReducers({todos});

const logger = createLogger({
    predicate: (getState, action) => process.env.NODE_ENV === "development"
});

const middleware = applyMiddleware(logger);

export default createStore(reducer, middleware)