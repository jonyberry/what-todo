# WhaTodo
Basic application for making todos.

## Built With
* bootstrapped with [Create React App](https://github.com/facebookincubator/create-react-app).

## How to use
* add new todo - type into top input, then press orange button
* remove todo - press icon with cross
* complete todo - press icon with check
* edit todo - doubleclick on text of todo, after making changes press save button

## Authors
* Jan B�rtl